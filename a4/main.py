import functions as f

def main():
    f.get_reqs()
    f.display_stats()

if __name__ == "__main__":
    main()
