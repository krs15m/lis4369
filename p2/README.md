# LIS4369 - P2

## Kenneth Sasser

### P2 # Requirements:



1. Backwards Data Analysis Program.
2. Provide Screenshots of Data Analysis and plot.



#### Assignment Screenshots:

*Screenshot of Data Analysis Running*

![Data Analysis Running](img/output.png)

*Screenshot of Data Analysis Plot*

![Data Analysis Plot](img/output2.png)