

# LIS4369 - A2

## Kenneth Sasser

### A2 # Requirements:



1. Backwards Engineer Payroll Calculator
2. Provide Screenshots of calculator
3. Provide links to bitbucket repos


#### Assignment Screenshots:

*Screenshot of Payroll Calculator with no overtime*:

![Payroll with no overtime](https://bitbucket.org/krs15m/lis4369/raw/b0bded8610ef46dd684849f5c7a2672489e093bb/a2/img/justpay.PNG)

*Screenshot of Payroll Calculator with overtime*:

![Payroll Calculator with overtime](https://bitbucket.org/krs15m/lis4369/raw/b0bded8610ef46dd684849f5c7a2672489e093bb/a2/img/withover.PNG)
