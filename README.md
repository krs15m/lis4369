

# LIS4369 - Extensible Enterprise Solutions

## Kenneth Sasser
### LIS4369 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
	- Install Python
	- Install R
	- Install R Studio
	- Create a1_tip_calculator application
	- Provide screenshots of installations
	- Create BitBucket repo
	- Complete BitBucket Tutorial
	- Provide Git command descriptions
2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Backwards Engineer Payroll Calculator
	- Provide Screenshots of calculator
	- Provide links to bitbucket repos
3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Backwards Engineer Painting Estimator
	- Provide Screenshots of Estimator running
4. [A4 README.md](a4/README.md "My A4 README.md file")
	- Code and run demo.py
	- Then use it to backward-engineer the screenshots below it.
5. [A5 README.md](a5/README.md "My A5 README.md file")
	- 1.Complete the Introduction to R Setup and Tutorial.
	- Code and run lis4369_a5.R.
	- Include 2 plot screenshots of a5 assignment.
	- Include 5 plot screenshots of R Tutorial.
6. [P1 README.md](p1/README.md "My P1 README.md file")
	- Backwards Engineer Data Analysis Program
	- Provide Screenshots of Data Analysis program running
	- Provide Screenshots of Data Analysis Plot Graph
7. [P2 README.md](p2/README.md "My P2 README.md file")
	- Backwards Engineer Data Analysis Program
	- Provide Screenshots of Data Analysis program running
	- Provide Screenshots of Data Analysis Plot Graph
