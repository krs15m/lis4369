def display_menu():
    print("Payroll Calculator")
    print()
    print("1. Must use float data type for user input.")
    print("2. Overtime rate: 1.5 times hourly rate (hours over 40).")
    print("3. Holiday rate: 2.0 times hourly rate(all holiday hours).")
    print("4. Must format currency with dollar sign, and round to two decimal places.")
    print("5. Create at least three functions that are called by the program:")
    print("     a. main(): calls at least two other funcitons.")
    print("     b. get_requirements(): displays the program requirements.")
    print("     c. calculate_payroll(): calculates an individual one-week paycheck.")
    print()
    print("User Input:")

def convert_pay():
    hours_worked = float(input("Enter hours worked: "))
    holiday_worked = float(input("Enter holiday hours: "))
    hourly_rate = float(input("Enter hourly pay rate: "))
    
    base_pay = float(hours_worked * hourly_rate)
    overtime_pay = float((hours_worked - 40) * (1.5 * hourly_rate))
    holiday_pay = float(holiday_worked * (2 * hourly_rate))
    gross_pay = float(base_pay + overtime_pay + holiday_pay)
    
    if hours_worked > 40:
        base_pay = 40 * hourly_rate
        holiday_pay = holiday_pay
        overtime_pay = overtime_pay
        gross_pay = base_pay + overtime_pay + holiday_pay
    elif hours_worked <= 40:
        base_pay = hours_worked * hourly_rate
        holiday_pay = holiday_pay
        overtime_pay = 0
        gross_pay = base_pay + holiday_pay
    
    print()
    print("Base:", '${:,.2f}'.format(base_pay))
    print("Overtime:", '${:,.2f}'.format(overtime_pay))
    print("Holiday:", '${:,.2f}'.format(holiday_pay))
    print("Gross:", '${:,.2f}'.format(gross_pay))
    return


    
def main():
    display_menu()
    convert_pay()


if __name__ == "__main__":
    main()