# LIS4369 - P1

## Kenneth Sasser

### P1 # Requirements:



1. Backwards Data Analysis Program.
2. Provide Screenshots of Data Analysis and plot.



#### Assignment Screenshots:

*Screenshot of Data Analysis Running*

![Data Analysis Running](https://bitbucket.org/krs15m/lis4369/raw/459007edf9485c9de6d4e60b82409185df372d3f/p1/img/output.png)

*Screenshot of Data Analysis Plot*

![Data Analysis Plot](img/output2.PNG)