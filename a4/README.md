# LIS4369 - A4

## Kenneth Sasser

### A4 # Requirements:



1. Code and run demo.py
2. Then use it to backward-engineer the screenshots below it.



#### Assignment Screenshots:

*Screenshot of get_reqs()*

![Screenshot of get_reqs()](img/a4.PNG)

*Screenshot of graph()*

![Screenshot of graph](https://bitbucket.org/krs15m/lis4369/raw/b337168b5ba9a16fc8b944d52752246b3db73bd0/a4/img/graph.png)