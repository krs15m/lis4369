# LIS4369 - A5

## Kenneth Sasser

### A5 # Requirements:



1. Complete the Introduction to R Setup and Tutorial.
2. Code and run lis4369_a5.R.
3. Include 2 plot screenshots of a5 assignment.
4. Include 5 plot screenshots of R Tutorial.



#### Assignment Screenshots:

*Screenshot of Rstudio running*

![Screenshot of Rstudio running](img/rstudio.PNG)

*Screenshot plot for A5*

![Screenshot plot for A5](img/a51.png)

*Screenshot plot for A5*


![Screenshot plot for A5](img/a52.png)


*Screenshot plot for Tutorial*

![Screenshot plot for Tutorial](img/1.PNG)

*Screenshot plot for Tutorial*

![Screenshot plot for Tutorial](img/2.PNG)


*Screenshot plot for Tutorial*

![Screenshot plot for Tutorial](img/3.PNG)

*Screenshot plot for Tutorial*

![Screenshot plot for Tutorial](img/4.PNG)

*Screenshot plot for Tutorial*

![Screenshot plot for Tutorial](img/5.PNG)