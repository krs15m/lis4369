

# LIS4369 - Extensible Enterprise Solutions

## Kenneth Sasser

### A1 # Requirements:


1. Distrubuted Version Control with Git and Bitbucket
2. Development Installations
3. Questions



> #### Git commands w/short descriptions:

1. git init - This command creates an empty git repository
2. git status - List the files you've changed and those you still need to add or commit
3. git add - Add one or more files to staging
4. git commit - Commit changes to head
5. git push - Send changes to the master branch of your remote repository
6. git pull - Fetch from and integrate with another repository or a local branch
7. git branch - List all the branches in your repo, and also tell you what branch you're currently in

#### Assignment Screenshots:



![IDLE running Tip Calc](img/idlecalc.PNG)

*IDLE running Tip Calc*:

![Visual Studio Code running Tip Calc](img/vsccalc.PNG)

*Screenshot of Visual Studio Code running Tip Calc*:



